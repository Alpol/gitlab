def carre(nombre):
    """mets au carré le nombre choisit
       param nombre(int,float)
       return(int,float)
       CU nombre>0

    """
    return nombre ** 2


import math

def pytagore(cote1, cote2):
    """calculel'hypothenuse d'un triangle
    param: cote1(int,float) nombre a choisir
    param: cote2(int,float) nombre a choisir
    return(int,float)l'hypothenuse
    CU: cote1 et 2 doivent être supérieur

    """
    assert cote1 >0
    assert cote2 >0
    hypothenuse= carre(cote1) + carre(cote2)
    # appliquer la modification précédente à ce qui précède
    hypothenuse = math.sqrt(hypothenuse)  # ici, on fait appel à la fonction dénommée sqrt
                                # qui est inclus dans le module importé math
    return hypothenuse




def moyenne_bac(note):
    """
    donne la mention en fonction de la moyenne
    param: (int,flaot) moyenne de l'élève
    return (string) mention de lélève

    """
    assert note >=0,"note doit être compris entre0 et 20"
   
    assert note <=20,"note doit être compris entre0 et 20" 
    
    if note >= 16:
        mention = "Mention TB !"
    elif note >= 14:
        mention = "Mention B !"
    elif note >= 12:
        mention = "Mention AB !"
    else:
        note = "Pas de mention !"
    return mention




def note_bac(moyenne):
    """
donne la mention en fonction de la moyenne
    param: (int,flaot) moyenne de l'élève
    return (string) mention de lélève

    """
    if moyenne >= 16:
        return "Mention TB !"
    if moyenne >= 14:
        return "Mention B !"
    if moyenne >= 12:
        return "Mention AB !"
    return "Pas de mention !"